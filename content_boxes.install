<?php
/**
 * @file
 * content boxes module that handles the creation of the content type and creation of boxes and putting them where they need to be put , with filtering and sorting.
 */
/**
 * Implements hook_install().
 * Add and configure fields on "box" content type.
 */
function content_boxes_install() {
  _content_boxes_install_type_create();
  _content_boxes_install_ct_configure();
}

/**
 * Create "content_box" content type
 * Add and configure fields on "content_box" content type.
 */
function _content_boxes_install_type_create() {
  $box_node_type = array(
    'type' => 'content_box',
    'name' => t('Box'),
    'base' => 'node_content',
    'description' => t('Boxes to be displayed as side content all around the site.'),
    'custom' => 1,
    'modified' => 1,
    'locked' => 1,
    'has_title' => '1',
    'title_label' => t('Title'),
    'help' => '',
  );

  $box_node_type = node_type_set_defaults($box_node_type);
  node_type_save($box_node_type);

  // body field
  node_add_body_field($box_node_type);
  // load the instance definition
  $body_instance = field_info_instance('node', 'body', 'content_box');
  // and configure it properly
  $body_instance['type'] = 'text_with_summary';
  // and save it back
  field_update_instance($body_instance);

  // create all other fields
  foreach (_content_boxes_fields() as $field) {
    field_create_field($field);
  }

  // create all instances of the fields
  foreach (_content_boxes_instances() as $instance) {
    $instance['entity_type'] = 'node';
    $instance['bundle'] = 'content_box';
    field_create_instance($instance);
  }
}

/**
 * Return a structured array of all field created by this content type.
 */
function _content_boxes_fields() {
  $t = get_t();

  return array(
    'box_c2a_label' => array(
      'field_name' => 'box_c2a_label',
      'label' => $t('Call to action label'),
      'description' => $t('Label for the call to action button, we recommand to use a short verb.  Call 2 action will not show if no call 2 action URL is specified.'),
      'type' => 'text',
      'translatable' => '1',
    ),

    'box_style' => array(
      'field_name' => 'box_style',
      'label' => $t('Choose the style to use to display this box.  Depending on the style all fields may not be used (i.e : an image only style may not show the call to action or the summary.'),
      'type' => 'list_text',
        'cardinality' => '1',
        'foreign keys' => array(),
        'indexes' => array(
          'value' => array(
            0 => 'value',
          ),
        ),
        'settings' => array(
          'allowed_values' => array(),
          'allowed_values_function' => 'content_boxes_get_styles_field_values',
        ),
        'translatable' => '1',
    ),
    
    'box_pages' => array(
      'field_name' => 'box_pages',
      'label' => $t('Pages'),
      'description' => $t('Specify pages by using their paths. Enter one path per line. The \'*\' character is a wildcard. Example paths are blog for the blog page and blog/* for every personal blog. <front> is the front page. No heading slash (\'/\').'),
      'type' => 'text_long',
      'translatable' => '1',
    ),

    'box_thumbnail' => array(
      'field_name' => 'box_thumbnail',
      'label' => $t('Thumbnail'),
      'description' => $t('Leave empty to ignore.'),
      'type' => 'image',
      'settings' => array(
        'default_image' => 0,
        'uri_scheme' => 'public',
      ),
      'translatable' => '1',
    ),

    'box_url' => array(
      'field_name' => 'box_url',
      'label' => $t('URL to which the box should link.  Leave empty to ignore.'),
      'type' => 'text',
      'settings' => array(
        'max_length' => '255',
      ),
      'translatable' => '1',
    ),

    'box_visibility' => array(
      'field_name' => 'box_visibility',
      'label' => $t('URL to which the box should link.  Leave empty to ignore.'),
      'cardinality' => 1,
      'type' => 'list_integer',
      'settings' => array(
        'allowed_values' => array(
          0 => 'All pages except those listed',
         1 => 'Only the listed pages',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '1',
    ),

    'box_zone' => array(
      'field_name' => 'box_zone',
      'label' => $t('Choose the zone where this box will be displayed.'),
	    'type' => 'list_text',
        'cardinality' => '1',
	      'foreign keys' => array(),
	      'indexes' => array(
	        'value' => array(
            0 => 'value',
          ),
        ),
	      'settings' => array(
	        'allowed_values' => array(),
	        'allowed_values_function' => 'content_boxes_get_zones_field_values',
        ),
	      'translatable' => '1',
    ),
	
    'box_weight' => array(
      'field_name' => 'box_weight',
      'label' => $t('Weight'),
      'cardinality' => 1,
      'type' => 'list_integer',
      'settings' => array(
        'allowed_values' => array(
          -20 => '-20',
          -19 => '-19',
          -18 => '-18',
          -17 => '-17',
          -16 => '-16',
          -15 => '-15',
          -14 => '-14',
          -13 => '-13',
          -12 => '-12',
          -11 => '-11',
          -10 => '-10',
          -9 => '-9',
          -8 => '-8',
          -7 => '-7',
          -6 => '-6',
          -5 => '-5',
          -4 => '-4',
          -3 => '-3',
          -2 => '-2',
          -1 => '-1',
          0 => '0',
          1 => '1',
          2 => '2',
          3 => '3',
          4 => '4',
          5 => '5',
          6 => '6',
          7 => '7',
          8 => '8',
          9 => '9',
          10 => '10',
          11 => '11',
          12 => '12',
          13 => '13',
          14 => '14',
          15 => '15',
          16 => '16',
          17 => '17',
          18 => '18',
          19 => '19',
          20 => '20',
        ),
        'allowed_values_function' => '',
      ),
      'translatable' => '1',
    ),
  );
}

/**
 * Return a structured array of all instances of the field listed in _content_boxes_fields
 */
function _content_boxes_instances() {
  $t = get_t();
  // bundle and entity_type are added by caller
  return array(
    'box_style' => array(
      'field_name' => 'box_style',
      'label' => $t('Style'),
      'required' => 1,
      'default_value' => NULL,
      'description' => $t('Choose the style to use to display this box.  Depending on the style all fields may not be used (i.e : an image only style may not show the call to action or the summary.'),
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '10',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '4',
        ),
      ),
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_select',
        'weight' => '1',
      ),
    ),
    
    'box_thumbnail' => array(
      'field_name' => 'box_thumbnail',
      'label' => $t('Thumbnail'),
      'description' => $t('Leave empty to ignore.'),
      'required' => 0,
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'image',
          'settings' => array(
            'image_link' => '',
            'image_style' => 'thumbnail',
          ),
          'type' => 'image',
          'weight' => '1',
        ),
      ),
      'entity_type' => 'node',
      'settings' => array(
        'alt_field' => 1,
        'file_directory' => '',
        'file_extensions' => 'png gif jpg jpeg',
        'max_filesize' => '',
        'max_resolution' => '',
        'min_resolution' => '',
        'title_field' => 1,
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'image',
        'settings' => array(
          'preview_image_style' => 'thumbnail',
          'progress_indicator' => 'throbber',
        ),
        'type' => 'image_image',
        'weight' => '2',
      ),
    ),
    
    'box_c2a_label' => array(
      'field_name' => 'box_c2a_label',
      'label' => $t('Call to action label'),
      'description' => $t('Label for the call to action button, we recommand to use a short verb.  Call 2 action will not show if no call 2 action URL is specified.'),
      'required' => 0,
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => '25',
        ),
        'type' => 'text_textfield',
        'weight' => '4',
      ),
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '3',
        ),
      ),
    ),

    'box_url' => array(
      'field_name' => 'box_url',
      'default_value' => NULL,
      'label' => $t('URL'),
      'description' => $t('URL to which the box should link.  Leave empty to ignore.'),
      'required' => 0,
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'module' => 'text',
          'settings' => array(),
          'type' => 'text_default',
          'weight' => '2',
        ),
      ),
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'size' => '60',
        ),
        'type' => 'text_textfield',
        'weight' => '3',
      ),
    ),
    
    'box_zone' => array(
      'field_name' => 'box_zone',
      'label' => $t('Zone'),
      'required' => 1,
      'default_value' => NULL,
      'description' => $t('Choose the zone where this box will be displayed.'),
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '10',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '4',
        ),
      ),
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_select',
        'weight' => '9',
      ),
    ),
    
    'box_visibility' => array(
      'field_name' => 'box_visibility',
      'label' => $t('Visibility'),
      'required' => 1,
      'default_value' => array(
        0 => array(
          'value' => '0',
        ),
      ),
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '7',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '8',
        ),
      ),
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_buttons',
        'weight' => '10',
      ),
    ),
    
    'box_pages' => array(
      'field_name' => 'box_pages',
      'label' => $t('Pages'),
      'required' => 0,
      'description' => $t('Specify pages by using their paths. Enter one path per line. The \'*\' character is a wildcard. Example paths are blog for the blog page and blog/* for every personal blog. <front> is the front page. No heading slash (\'/\').'),
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '6',
        ),
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'text',
        'settings' => array(
          'rows' => '5',
        ),
        'type' => 'text_textarea',
        'weight' => '11',
      ),
      'settings' => array(
        'text_processing' => '0',
        'user_register_form' => FALSE,
      ),
    ),

    
    'box_weight' => array(
      'field_name' => 'box_weight',
      'label' => $t('Weight'),
      'required' => 1,
      'default_value' => array(
        0 => array(
          'value' => '0',
        ),
      ),
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'hidden',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '8',
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => '7',
        ),
      ),

      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'options',
        'settings' => array(),
        'type' => 'options_select',
        'weight' => '12',
      ),
    ),
  );
}

function _content_boxes_install_ct_configure() {
  variable_set('comment_content_box', 1);
  variable_set('i18n_node_extended_content_box', 1);
  variable_set('i18n_node_options_content_box', array(0 => 'current', 1 => 'required'));
  variable_set('language_content_type_content_box', 2);
  variable_set('menu_options_content_box', array());
  variable_set('node_options_content_box', array(0 => 'status'));
}

/**
 * Implements hook_uninstall().
 */
function content_boxes_uninstall() {
  // remove existing content
  $sql = "SELECT nid FROM {node} AS n WHERE n.type = :type";
  $result = db_query($sql, array(':type' => 'content_box'));
  $nids = array();
  foreach ($result as $row) {
    $nids[] = $row->nid;
  }

  node_delete_multiple($nids);
  // delete fields
  foreach (array_keys(_content_boxes_fields()) as $field) {
    field_delete_field($field);
  }
  // delete instances
  $instances = field_info_instances('node', 'content_box');
  foreach ($instances as $name => $instance) {
    field_delete_instance($instance);
  }
  // delete CT
  node_type_delete('content_box');

  field_purge_batch(1000);

  // then remove variables
  variable_del('comment_content_box');
  variable_del('i18n_node_extended_content_box');
  variable_del('i18n_node_options_content_box');
  variable_del('language_content_type_content_box');
  variable_del('menu_options_content_box');
  variable_del('node_options_content_box');
  variable_det('content_boxes_search_exclude');
}