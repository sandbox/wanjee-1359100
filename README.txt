
After installing this module you can 

- Implement hook_content_boxes_zones_alter to add you own zones or modify the existing.
  For each defined zone a new block will be automatically generated and the content editor will be able to choose it 
  as a destination for his box.
  
  Example : 
  /**
   * Implements of hook_content_boxes_styles_alter().
   */
  function site_helper_content_boxes_zones_alter(&$items) {
    
    // add a new zone
    $items['highlight'] = array(
      '#title' => 'Highlight',
      '#weight' => 3,
    );
    
    // change weight of one of the default zones (weight is only usefull on node edit form)
    $items['content']['#weight'] = 5;
  }
  

- Implement hook_content_boxes_styles_alter to add your own styles or modify the existing ones.
  
  Example : 
  /**
   * Implementats of hook_content_boxes_styles_alter().
   */
  function site_helper_content_boxes_styles_alter(&$items) {
    // remove a default style
    unset($items['c2a']);

    // add a new style
    $items['bigger_c2a'] = array(
      '#title' => t('Big call to action'),
      '#weight' => 10, // order in list in node edit form
      '#hide_title' => TRUE, // hidden by CSS
      '#theme_callback' => 'your_module_bigcall2action', // require your module to define theme_your_module_bigcall2action($vars) 
    );
  }
  
  As alternate style use theme functions you can also juste override the output of those theme function by implementing them in your theme
  
- Theme content boxes.  The boxes are rendered using node_view_multiple, the viewmode is "teaser".  
  When no #theme_callback is specified you should therefore theme the teaser view mode of the box content type so that they render as block when loaded in their corresponding zone.
  Theme the full view mode only if you want blocks to be accessible as full page (can be usefull if you chose not to hide them from search engine)

- Configure zones (blocks containing boxes) to be added in the proper theme regions using core block interface or context.  You can configure several zones to be displayed in a same region.
  There is no relation between the zone name and the region in which it will be displayed.  Zone names should only be meaningfull for the editor.
  
- Configure Box content type settings : i18n support, comments settings, available menu,...

- In settings you can specify if the content_box nodes should be hidden from the search engine. 


This module implements the field_group module hook : http://drupal.org/project/field_group

