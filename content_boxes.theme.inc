<?php
/**
* @file
* this file themes the content management form.
*/
/**
 * Theme the featured content management form.
 * @param array $form
 */
function theme_content_boxes_box_table($variables) {
  $form = $variables['form'];
  
  drupal_add_tabledrag('draggable-table', 'order', 'sibling', 'box-weight');
   
  $header = array(t('Title'), t('Zone'), t('Weight'), t('Language'), t('Published'), array('data' => t('Operations'), 'colspan' => '2'));
  $rows = array();
  
  $destination = drupal_get_destination();
  
  foreach (element_children($form) as $key) {
    $element = &$form[$key];
    $nid = $element['nid']['#value'];
    
    $element['weight']['#attributes']['class'] = array('box-zone');
    $element['weight']['#attributes']['class'] = array('box-weight');

    $data = array(
      drupal_render($element['title']),
      drupal_render($element['zone']),
      drupal_render($element['weight']),
      drupal_render($element['language']),
      drupal_render($element['status']),
      l(t('edit'), 'node/' . $nid . '/edit', array('query' => $destination)),
      l(t('delete'), 'node/' . $nid . '/delete', array('query' => $destination)),
    );
    $row = array('data' => $data);
    if (isset($element['#attributes'])) {
      $row = array_merge($row, $element['#attributes']);
    }
    $row['class'][] = 'draggable';
    $rows[] = $row;
  }

  return theme('table', array('header' => $header, 'rows' => $rows, 'attributes' => array('id' => 'draggable-table')));
}


/**
 * Theme content_box node as a banner
 * @param array $vars
 */
function theme_content_boxes_banner($vars) {
  $node = $vars['node'];
  
  if (!empty($node->box_thumbnail[$node->language][0]['uri'])) {
  
    $image = theme('image_style', array(
      'style_name' => 'medium',
      'alt' => $node->title,
      'path' => $node->box_thumbnail[$node->language][0]['uri'],
    ));
    
    if (!empty($node->box_url[$node->language][0]['value'])) {
      return l($image, $node->box_url[$node->language][0]['value'], array('html' => TRUE, 'attributes' => array('class' => 'call2action')));
    }
    else {
      return $image;
    }
  }
  else {
    return '';
  }
}

/**
 * Theme content_box node as a call 2 action
 * @param array $vars
 */
function theme_content_boxes_call2action($vars) {
  $node = $vars['node'];
  
  if (!empty($node->box_url[$node->language][0]['value'])) {
    if (!empty($node->box_c2a_label[$node->language][0]['value'])) {
      $label = $node->box_c2a_label[$node->language][0]['value'];
    }
    else {
      $label = $node->title;
    }
    
    
    return l($node->box_c2a_label[$node->language][0]['value'], $node->box_url[$node->language][0]['value'], array('attributes' => array('class' => 'call2action')));
  }
  else {
    return '';
  }
}
